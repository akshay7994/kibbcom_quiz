#ifndef QUIZSTACKPROJECT17_H
#define QUIZSTACKPROJECT17_H
#include <QRadioButton>

#include <QMainWindow>

namespace Ui {
class QuizStackProject17;
}

class QuizStackProject17 : public QMainWindow
{
    Q_OBJECT

public:
    explicit QuizStackProject17(QWidget *parent = 0);
    ~QuizStackProject17();
    int randInt(int low, int high);
    int numberCount = 0;

//    QRadioButton *radio1;
//    QRadioButton *radio2;
//    QRadioButton *radio3;

    int min,sec,hr;
    bool eventFilter(QObject *object, QEvent *event);



private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    //void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    //void on_pushButton_7_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_7_clicked();
    void update();

    void on_pushButton_8_clicked();

    void on_radioButton_clicked() ;

    void on_radioButton_2_clicked();

    void on_radioButton_3_clicked();

    void on_radioButton_4_clicked();

    void on_radioButton_5_clicked();

    void on_radioButton_6_clicked();



private:
    Ui::QuizStackProject17 *ui;
    int i=4;
    int ques=0;
    int minute, hour, seconds;
    QString name,name1, email, phone,firstname,lastname,password,confirmpassword,Ansr,ans;
    int cnt = 1;
    bool button=false;
    int randomValue;

    QTimer *timer;
    int tim =0;
    QString r1,r2,r3,r4,r5,r6;

};

#endif // QUIZSTACKPROJECT17_H
