#include "quizstackproject17.h"
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QLinearGradient>
#include <QMessageBox>
#include <QPushButton>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QStackedWidget>
#include <QTime>
#include <cstdlib>
#include <QtGui>
#include "ui_quizstackproject17.h"
#include <QTimer>
#include <QElapsedTimer>

QuizStackProject17::QuizStackProject17(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QuizStackProject17)
{
    ui->setupUi(this);
    ui->stackedWidget->installEventFilter(this);
    ui->stackedWidget->setCurrentIndex(0); // call to registration button

    ui->pushButton_4->setVisible(false);
    ui->pushButton->setStyleSheet("background-color: rgb(176,196,222)");
    ui->pushButton_2->setStyleSheet("background-color: rgb(176,196,222)");
    ui->pushButton_3->setStyleSheet("background-color: rgb(176,196,222)");
    ui->pushButton_4->setStyleSheet("background-color: rgb(176,196,222)");
    ui->pushButton_5->setStyleSheet("background-color: rgb(176,196,222)");
    ui->pushButton_6->setStyleSheet("background-color: rgb(176,196,222)");
    ui->pushButton_7->setStyleSheet("background-color: rgb(176,196,222)");
    // ui->stackedWidget->setStyleSheet("qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0
    // rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255))");

    // for registration page
    ui->lineEdit->setMaxLength(50);
    ui->lineEdit_2->setMaxLength(50);
    ui->lineEdit_3->setMaxLength(10);
    ui->lineEdit_3->setValidator(new QDoubleValidator(0, 10, 9, this));
    ui->lineEdit_3->setPlaceholderText("hello");
    ui->lineEdit_4->setMaxLength(50);
    ui->lineEdit_5->setMaxLength(50);
    ui->lineEdit_4->setPlaceholderText("First Name");
    ui->lineEdit_5->setPlaceholderText("Surname");
    ui->lineEdit_2->setPlaceholderText("Enter email address");
    ui->lineEdit_3->setPlaceholderText("Enter mobile no.");
    ui->lineEdit->setPlaceholderText("Enter full name");
    ui->lineEdit_6->setMaxLength(15);
    ui->lineEdit_7->setMaxLength(15);
    ui->lineEdit_7->setPlaceholderText("Enter password");
    ui->lineEdit_6->setPlaceholderText("Re-enter password");

    //visibility of time
    ui->label_8->setVisible(false);
    ui->label_9->setVisible(false);
    ui->label_10->setVisible(false);
    ui->label_11->setVisible(false);
    ui->label_12->setVisible(false);

    //visibility of radio button
    ui->radioButton->setVisible(false);
    ui->radioButton_2->setVisible(false);
    ui->radioButton_3->setVisible(false);

    //time counter
    //QTimer::singleShot(1, this, SLOT(showFullScreen()));
    //ui->widget->installEventFilter(this);
    timer = new QTimer(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    min =0;
    hr=0;
    sec=0;
    numberCount=0;





    // for login page

    // code for random number generator
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
}

QuizStackProject17::~QuizStackProject17()
{
    delete ui;
}

int QuizStackProject17::randInt(int low, int high)
{
    // Random number between low and high
    return qrand() % ((high + 1) - low) + low;
}

void QuizStackProject17::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1); // call to login button

       button=true;
       name=ui->lineEdit->text();
       email=ui->lineEdit_2->text();
       phone=ui->lineEdit_3->text();
       firstname=ui->lineEdit_4->text();
       lastname=ui->lineEdit_5->text();
       password=ui->lineEdit_7->text();
       confirmpassword=ui->lineEdit_6->text();

       QRegExp mail("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
       mail.setCaseSensitivity(Qt::CaseInsensitive);
       mail.setPatternSyntax(QRegExp::RegExp);

       if(name=="" ||email=="" || phone=="" || firstname=="" || lastname=="" || password=="" ||
       confirmpassword=="")
       {
           QMessageBox::information(this, "Error Message","Name or Email or Phone No or password
           is Empty",QMessageBox::Ok,QMessageBox::Cancel);
       }
       else
       {
           if(mail.exactMatch(email))
           {

               QSqlQuery query;
             query.prepare("INSERT INTO Empl
             VALUES('"+name+"','"+firstname+"','"+lastname+"','"+password+"','"+confirmpassword+"','"+email+"',0,'User','"+phone+"')");
                //query.addBindValue(phone.toInt());

               if(!query.exec())
                   qWarning() << "ERROR: " << query.lastError().text();
               query.prepare("select * from Empl");
               if(!query.exec())
                   qWarning() << "ERROR: " << query.lastError().text();

               while(query.next())
               {

                   qDebug()<<
                   query.value(0).toString()<<query.value(3).toString()<<query.value(7).toString();

               }

           if(password==confirmpassword)
           {

               ui->stackedWidget->setCurrentIndex(1);

              //lp->sendData(name, password);
              //this->close();
           }
           else
           {
               QMessageBox::information(this, "Error Massage","Confirm password shoud be
               same",QMessageBox::Ok);
           }

           }

           else
           {
               QMessageBox::information(this, "Error Message","Invalid Email
               ID",QMessageBox::Ok,QMessageBox::Cancel);
           }

       }
}

void QuizStackProject17::update()
{
        tim++;
        sec=(tim)%60;
        min=((tim/60))%60;
        hr=((tim/60)/60)%24;
        if(sec<10)
        {
            ui->label_13->setText("0"+QString::number(sec));
        }
        else{
            ui->label_13->setText(QString::number(sec));
        }
        if(min<10)
        {
            ui->label_11->setText("0"+QString::number(min));
        }
        else
        {
           ui->label_11->setText(QString::number(min));
        }
        if(hr<10)
        {
            ui->label_9->setText("0"+QString::number(hr));
        }
        else
        {
           ui->label_9->setText(QString::number(hr));
        }

        if(min==45)
        {
            timer->stop();
            ui->stackedWidget->setCurrentIndex(5);

        }
}

bool QuizStackProject17::eventFilter(QObject *object, QEvent *event)
{
        if (event->type() == QEvent::WindowDeactivate)
         {
             if(cnt<3)
             {
                 QMessageBox::information(this, "Warning Message","Do not go out of app",QMessageBox::Ok);
                 cnt++;
             }
             else
             {
                 ui->stackedWidget->setCurrentIndex(5);

             }
         }
    return false;
}


void QuizStackProject17::on_pushButton_2_clicked()
{
    ui->stackedWidget->setCurrentIndex(3); // call to instruction button
    ui->pushButton_4->setVisible(true);
}


void QuizStackProject17::on_pushButton_4_clicked()
{
    timer->start(1000);



    //button =true;
    if(Ansr!="" && ans!="")
    {
        if(Ansr==ans)
        {
            numberCount++;
        }
    }

    qDebug()<<"Counter :" <<numberCount;

    // read data from JSON document
    QString settings;
    QFile file;
    file.setFileName("C:/Users/Samsung/Desktop/Question.json");
    file.open(QIODevice::ReadOnly | QIODevice::Text);

    QByteArray jsonData = file.readAll();
    file.close();

    //    settings = file.readAll();
    //    file.close();

    QJsonDocument sd = QJsonDocument::fromJson(jsonData);
    QJsonObject object = sd.object();
    // qWarning() << sd.isEmpty(); // <- print false
    QJsonValue value = object.value("Questions");
    QJsonArray array = value.toArray();
    // foreach (const QJsonValue & v, array)
    qDebug() << array.at(0).toObject().value("Question").toString();
    randomValue = randInt(0, 19);
    QString ques1 = array.at(randomValue).toObject().value("Question").toString();
    QString options1 = array.at(randomValue).toObject().value("Option1").toString();
    QString options2 = array.at(randomValue).toObject().value("Option2").toString();
    QString options3 = array.at(randomValue).toObject().value("Option3").toString();
    QString QNo = array.at(randomValue).toObject().value("QNo.").toString();
    Ansr = array.at(randomValue).toObject().value("Ans").toString();
    qDebug()<<"Answer : "<<Ansr;
//qDebug() << array.at(randomValue).toObject().value("Options").toString();




    if (ques <= 9)
    {
        if (i == 4)
        {

            qDebug() << randomValue;
            i = 3;





            //radio button visibility
            ui->radioButton->setVisible(true);
            ui->radioButton_2->setVisible(true);
            ui->radioButton_3->setVisible(true);

            //time visible
            ui->label_8->setVisible(true);
            ui->label_9->setVisible(true);
            ui->label_9->setText(QString::number(hr));
            ui->label_10->setVisible(true);
            ui->label_11->setVisible(true);
            ui->label_11->setText(QString::number(min));
            ui->label_12->setVisible(true);
            ui->label_13->setVisible(true);
            ui->label_13->setText(QString::number(sec));

            ui->label_4->setText(QNo);
            ui->label_22->setText(ques1);

            ui->radioButton->setText(options1);
            ui->radioButton_2->setText(options2);
            ui->radioButton_3->setText(options3);


            qDebug()<<Ansr;
            if(Ansr==ans)
            {
                numberCount++;
            }
            qDebug()<<numberCount;
            //ui->label->setText("QJsonObject")

            ui->stackedWidget->setCurrentIndex(i);
            ui->radioButton_4->setAutoExclusive(false);
            ui->radioButton_4->setChecked(false);
            ui->radioButton_4->setAutoExclusive(true);
            ui->radioButton_5->setAutoExclusive(false);
            ui->radioButton_5->setChecked(false);
            ui->radioButton_5->setAutoExclusive(true);
            ui->radioButton_6->setAutoExclusive(false);
            ui->radioButton_6->setChecked(false);
            ui->radioButton_6->setAutoExclusive(true);


        }
        else                                    // else part to handle next widget
        {

            qDebug() << randomValue;
            i = 4;
            ui->label_5->setText(QNo);
            ui->label_21->setText(ques1);

            ui->radioButton_4->setText(options1);
            ui->radioButton_5->setText(options2);
            ui->radioButton_6->setText(options3);

            qDebug()<<Ansr;


            qDebug()<<numberCount;
            ui->stackedWidget->setCurrentIndex(i);
            ui->radioButton->setAutoExclusive(false);
            ui->radioButton->setChecked(false);
            ui->radioButton->setAutoExclusive(true);
            ui->radioButton_2->setAutoExclusive(false);
            ui->radioButton_2->setChecked(false);
            ui->radioButton_2->setAutoExclusive(true);
            ui->radioButton_3->setAutoExclusive(false);
            ui->radioButton_3->setChecked(false);
            ui->radioButton_3->setAutoExclusive(true);

        }
    ques++;

    }

    else
    {

        ui->stackedWidget->setCurrentIndex(5); // call to submit button
        timer->stop();
        hour = hr;
        minute = min;
        seconds = sec;
        hr=0;
        min=0;
        sec=0;
        ques=0;
        ui->pushButton_4->setVisible(false);

    }
    ans="";
}



void QuizStackProject17::on_pushButton_5_clicked()
{

    ui->pushButton_4->setVisible(false);
    ui->label_8->setVisible(false);
    ui->label_9->setVisible(false);
    //ui->label_9->setText(QString::number(hr));
    ui->label_10->setVisible(false);
    ui->label_11->setVisible(false);
    //ui->label_11->setText(QString::number(min));
    ui->label_12->setVisible(false);
    ui->label_13->setVisible(false);
    ui->stackedWidget->setCurrentIndex(6); // call to result page

    ui->label_39->setText(QString::number(numberCount)+"/10");
    ui->label_41->setText(QString::number(hour));
    ui->label_43->setText(QString::number(minute));
    ui->label_45->setText(QString::number(seconds));

}

void QuizStackProject17::on_pushButton_6_clicked()
{
    numberCount=0;
    //timer->start(1000);
    timer->stop();
    tim=0;
    hr=0;
    min=0;
    sec=0;
    ques=0;
    ui->stackedWidget->setCurrentIndex(0);
    ui->label_4->setText("instruction");
    ui->label_22->setText("instruction are there");

    ui->radioButton->setVisible(false);
    ui->radioButton_2->setVisible(false);
    ui->radioButton_3->setVisible(false);
}

void QuizStackProject17::on_pushButton_7_clicked()
{
    this->close(); // close the app
}


void QuizStackProject17::on_pushButton_8_clicked()
{

    ui->stackedWidget->setCurrentIndex(0);
}


void QuizStackProject17::on_radioButton_clicked()
{
    ans=ui->radioButton->text();
    qDebug()<<ans;
}

void QuizStackProject17::on_radioButton_2_clicked()
{
    ans=ui->radioButton_2->text();
    qDebug()<<ans;
}

void QuizStackProject17::on_radioButton_3_clicked()
{
    ans=ui->radioButton_3->text();
    qDebug()<<ans;
}

void QuizStackProject17::on_radioButton_4_clicked()
{

    ans=ui->radioButton_4->text();
    qDebug()<<ans;
}

void QuizStackProject17::on_radioButton_5_clicked()
{

    ans=ui->radioButton_5->text();
    qDebug()<<ans;
}

void QuizStackProject17::on_radioButton_6_clicked()
{

    ans=ui->radioButton_6->text();
    qDebug()<<ans;
}
